<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <atom:link href="https://www.journal-theme.com/5d/index.php?route=journal3/blog/feed" rel="self" type="application/rss+xml"/>
    <title>Journal Blog</title>
    <link>https://www.journal-theme.com/5d/index.php?route=journal3/blog</link>
    <description></description>

          <item>
        <title>Vacation Time</title>
                  <author>journal@digitalatelier.com (admin)</author>
                <pubDate>Thu, 02 Aug 2018 12:30:49 +0000</pubDate>
        <link>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=12</link>
        <guid>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=12</guid>
        <description>
          <![CDATA[
                      <p><img src="https://www.journal-theme.com/5d/image/cache/catalog/journal3/gallery/backdrop023-335x200h.jpg" alt="Vacation Time"/></p>
                    Vacations are so much fun. The Journal 3 blog has been greatly improved and it now comes with the most advanced set of typography tools, including custom drop-cap support as well as optional newspaper..
          <a href="https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=12">Read More</a>
          ]]>
        </description>
      </item>
          <item>
        <title>Best Leather Bags</title>
                  <author>journal@digitalatelier.com (admin)</author>
                <pubDate>Thu, 02 Aug 2018 12:29:24 +0000</pubDate>
        <link>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=11</link>
        <guid>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=11</guid>
        <description>
          <![CDATA[
                      <p><img src="https://www.journal-theme.com/5d/image/cache/catalog/journal3/gallery/demo28-335x200h.jpg" alt="Best Leather Bags"/></p>
                    Best Opencart theme options you can find in any theme? Decide for yourself by visiting one our demo admin, user/pass: demo/demo. The Journal 3 blog has been greatly improved and it now comes with the ..
          <a href="https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=11">Read More</a>
          ]]>
        </description>
      </item>
          <item>
        <title>Another Blog Post</title>
                  <author>journal@digitalatelier.com (admin)</author>
                <pubDate>Thu, 02 Aug 2018 12:28:14 +0000</pubDate>
        <link>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=10</link>
        <guid>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=10</guid>
        <description>
          <![CDATA[
                      <p><img src="https://www.journal-theme.com/5d/image/cache/catalog/journal3/gallery/backdrop036-335x200h.jpg" alt="Another Blog Post"/></p>
                    Another blog post.  Write unlimited blog articles, or have someone write them for you with partial admin access. The Journal 3 blog has been greatly improved and it now comes with the most advanced se..
          <a href="https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=10">Read More</a>
          ]]>
        </description>
      </item>
          <item>
        <title>Traveling to Greece</title>
                  <author>journal@digitalatelier.com (admin)</author>
                <pubDate>Thu, 26 Jul 2018 17:43:21 +0000</pubDate>
        <link>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=9</link>
        <guid>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=9</guid>
        <description>
          <![CDATA[
                      <p><img src="https://www.journal-theme.com/5d/image/cache/catalog/journal3/gallery/wing-335x200h.jpg" alt="Traveling to Greece"/></p>
                    Travel in peace with the new scheduling options. The new Schedule feature allows you to display any module at specific dates in the future, or to disable any module automatically at a certain time and..
          <a href="https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=9">Read More</a>
          ]]>
        </description>
      </item>
          <item>
        <title>Welcome to Journal Blog</title>
                  <author>journal@digitalatelier.com (admin)</author>
                <pubDate>Mon, 15 Sep 2014 12:06:26 +0000</pubDate>
        <link>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=1</link>
        <guid>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=1</guid>
        <description>
          <![CDATA[
                      <p><img src="https://www.journal-theme.com/5d/image/cache/catalog/journal3/gallery/photographer-335x200h.jpg" alt="Welcome to Journal Blog"/></p>
                    Journal 3 blog has been greatly improved and it now comes with the most advanced set of typography tools, including custom drop-cap support as well as optional newspaper-like fluid columns. You can br..
          <a href="https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=1">Read More</a>
          ]]>
        </description>
      </item>
          <item>
        <title>Season Essentials</title>
                  <author>journal@digitalatelier.com (admin)</author>
                <pubDate>Mon, 15 Sep 2014 12:06:26 +0000</pubDate>
        <link>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=3</link>
        <guid>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=3</guid>
        <description>
          <![CDATA[
                      <p><img src="https://www.journal-theme.com/5d/image/cache/catalog/journal3/gallery/pack-335x200h.jpg" alt="Season Essentials"/></p>
                    Seasons come and go but Journal is here to stay. The Journal 3 blog has been greatly improved and it now comes with the most advanced set of typography tools, including custom drop-cap support as well..
          <a href="https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=3">Read More</a>
          ]]>
        </description>
      </item>
          <item>
        <title>Best Beauty Products</title>
                  <author>journal@digitalatelier.com (admin)</author>
                <pubDate>Mon, 15 Sep 2014 12:06:26 +0000</pubDate>
        <link>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=4</link>
        <guid>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=4</guid>
        <description>
          <![CDATA[
                      <p><img src="https://www.journal-theme.com/5d/image/cache/catalog/journal3/gallery/backdrop031-335x200h.jpg" alt="Best Beauty Products"/></p>
                    Best Opencart theme around, period. The Journal 3 blog has been greatly improved and it now comes with the most advanced set of typography tools, including custom drop-cap support as well as optional ..
          <a href="https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=4">Read More</a>
          ]]>
        </description>
      </item>
          <item>
        <title>Introducing our Summer Dresses</title>
                  <author>journal@digitalatelier.com (admin)</author>
                <pubDate>Mon, 15 Sep 2014 12:06:26 +0000</pubDate>
        <link>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=7</link>
        <guid>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=7</guid>
        <description>
          <![CDATA[
                      <p><img src="https://www.journal-theme.com/5d/image/cache/catalog/journal3/gallery/backdrop015-335x200h.jpg" alt="Introducing our Summer Dresses"/></p>
                    Introducing the new summer wears. The Journal 3 blog has been greatly improved and it now comes with the most advanced set of typography tools, including custom drop-cap support as well as optional ne..
          <a href="https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=7">Read More</a>
          ]]>
        </description>
      </item>
          <item>
        <title>The Wool Jackets Are Back</title>
                  <author>journal@digitalatelier.com (admin)</author>
                <pubDate>Mon, 15 Sep 2014 12:06:26 +0000</pubDate>
        <link>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=8</link>
        <guid>https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=8</guid>
        <description>
          <![CDATA[
                      <p><img src="https://www.journal-theme.com/5d/image/cache/catalog/journal3/gallery/gent-335x200h.jpg" alt="The Wool Jackets Are Back"/></p>
                    Wool jackets are so warm. The Journal 3 blog has been greatly improved and it now comes with the most advanced set of typography tools, including custom drop-cap support as well as optional newspaper-..
          <a href="https://www.journal-theme.com/5d/index.php?route=journal3/blog/post&amp;journal_blog_post_id=8">Read More</a>
          ]]>
        </description>
      </item>
    
  </channel>
</rss>
